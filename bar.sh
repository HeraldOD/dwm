#!/bin/dash
# simple dwm bar script

export XDG_CURRENT_DESKTOP="dwm"

while true; do
    xsetroot -name " $(battery || echo "AC")▕ $(date +%H:%M) "
    sleep 5s
done
