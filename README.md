# DWM - My Personal Fork

## Build

```sh
sudo make clean install
```

## Run

Add to `~/.xinitrc`:

```sh
repo/folder/bar.sh &
exec dwm
```

## Configure

Edit the `config.h` file.

## Patches

- TODO

## Get Upstream Changes

Assuming you have a remote called 'upstream' (https://git.suckless.org/dwm):

```sh
git fetch upstream
git rebase upstream/master
git push --force-with-lease
```
